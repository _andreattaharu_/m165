const express = require("express");
const bodyParser = require("body-parser");
const cassandra = require("cassandra-driver");
const path = require("path");
const app = express();
const port = 3000;

const client = new cassandra.Client({
  contactPoints: ["localhost"],
  localDataCenter: "datacenter1",
  keyspace: "rezepte",
});

client
  .connect()
  .then(() => console.log("Connected to Cassandra"))
  .catch((err) => console.error("Connection error:", err));

app.use(bodyParser.json());

// Serve static files from the "public" directory
app.use(express.static(path.join(__dirname, "public")));

// Add a route for the root URL
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

app.get("/rezepte", async (req, res) => {
  const query = "SELECT * FROM rezepte";
  try {
    const result = await client.execute(query);
    res.json(result.rows);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.post("/rezepte", async (req, res) => {
  const { name, kategorie, anleitung } = req.body;
  const query =
    "INSERT INTO rezepte (rezept_id, name, kategorie, anleitung) VALUES (uuid(), ?, ?, ?)";
  try {
    await client.execute(query, [name, kategorie, anleitung], {
      prepare: true,
    });
    res.status(201).send("Rezept hinzugefügt");
  } catch (err) {
    res.status(500).send(err);
  }
});

app.delete("/rezepte/:id", async (req, res) => {
  const id = req.params.id;
  const query = "DELETE FROM rezepte WHERE rezept_id = ?";
  try {
    await client.execute(query, [id], { prepare: true });
    res.status(200).send("Rezept gelöscht");
  } catch (err) {
    res.status(500).send(err);
  }
});

app.listen(port, () => {
  console.log(`Server läuft auf http://localhost:${port}`);
});
