#!/bin/bash

# Update package list and install Java
sudo apt update
sudo apt install -y openjdk-11-jdk

# Add Cassandra repository and install Cassandra
echo "deb http://www.apache.org/dist/cassandra/debian 311x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list
curl https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -
sudo apt update
sudo apt-get install cassandra

# Start Cassandra service
sudo systemctl start cassandra
sudo systemctl enable cassandra

echo "Cassandra installation completed."
