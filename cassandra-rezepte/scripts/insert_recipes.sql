USE rezept_sammlung;

-- Insert new recipes into rezepte
INSERT INTO rezepte (rezept_id, name, kategorie, anleitung) VALUES (uuid(), 'Lasagne', 'Hauptgericht', '1. Fleischsoße zubereiten. 2. Bechamelsoße zubereiten. 3. Lasagneblätter in die Form schichten. 4. Abwechselnd Soßen und Blätter schichten. 5. Mit Käse abschließen und backen.');
INSERT INTO rezepte (rezept_id, name, kategorie, anleitung) VALUES (uuid(), 'Caesar Salad', 'Salat', '1. Salatblätter waschen und trocknen. 2. Hähnchenbrust anbraten und in Streifen schneiden. 3. Brot in Würfel schneiden und rösten. 4. Dressing aus Olivenöl, Zitronensaft, Knoblauch, Dijon-Senf und Parmesan zubereiten. 5. Alles mischen und mit Parmesan bestreuen.');
INSERT INTO rezepte (rezept_id, name, kategorie, anleitung) VALUES (uuid(), 'Chili con Carne', 'Hauptgericht', '1. Zwiebeln und Knoblauch anbraten. 2. Hackfleisch hinzufügen und anbraten. 3. Tomaten, Bohnen und Gewürze hinzufügen. 4. Köcheln lassen, bis alles gut durchgezogen ist.');
INSERT INTO rezepte (rezept_id, name, kategorie, anleitung) VALUES (uuid(), 'Schoko-Brownies', 'Dessert', '1. Butter und Schokolade schmelzen. 2. Zucker und Eier verrühren. 3. Geschmolzene Schokolade unterrühren. 4. Mehl, Kakao und Backpulver hinzufügen. 5. In eine Form geben und backen.');
INSERT INTO rezepte (rezept_id, name, kategorie, anleitung) VALUES (uuid(), 'Tomatensuppe', 'Vorspeise', '1. Zwiebeln und Knoblauch anbraten. 2. Tomaten und Brühe hinzufügen. 3. Kochen lassen, bis die Tomaten weich sind. 4. Pürieren und abschmecken.');
