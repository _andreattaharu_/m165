-- Create keyspace
CREATE KEYSPACE IF NOT EXISTS rezept_sammlung
WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };

-- Use keyspace
USE rezept_sammlung;

-- Create tables
CREATE TABLE IF NOT EXISTS rezepte (
    rezept_id UUID PRIMARY KEY,
    name TEXT,
    kategorie TEXT,
    anleitung TEXT
);

CREATE TABLE IF NOT EXISTS zutaten (
    zutat_id UUID PRIMARY KEY,
    rezept_id UUID,
    name TEXT,
    menge TEXT
);
