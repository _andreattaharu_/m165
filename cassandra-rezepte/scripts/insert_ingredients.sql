USE rezept_sammlung;

-- Lasagne
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a680-5d3e-11ec-bf63-0242ac130002, 'Hackfleisch', '500g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a680-5d3e-11ec-bf63-0242ac130002, 'Lasagneblätter', '200g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a680-5d3e-11ec-bf63-0242ac130002, 'Tomatensoße', '500ml');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a680-5d3e-11ec-bf63-0242ac130002, 'Bechamelsoße', '500ml');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a680-5d3e-11ec-bf63-0242ac130002, 'Käse', '200g');

-- Caesar Salad
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a681-5d3e-11ec-bf63-0242ac130002, 'Salatblätter', '200g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a681-5d3e-11ec-bf63-0242ac130002, 'Hähnchenbrust', '150g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a681-5d3e-11ec-bf63-0242ac130002, 'Brotwürfel', '50g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a681-5d3e-11ec-bf63-0242ac130002, 'Parmesan', '30g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a681-5d3e-11ec-bf63-0242ac130002, 'Dressing', '100ml');

-- Chili con Carne
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a682-5d3e-11ec-bf63-0242ac130002, 'Hackfleisch', '500g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a682-5d3e-11ec-bf63-0242ac130002, 'Tomaten', '400g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a682-5d3e-11ec-bf63-0242ac130002, 'Bohnen', '400g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a682-5d3e-11ec-bf63-0242ac130002, 'Zwiebeln', '2 Stück');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a682-5d3e-11ec-bf63-0242ac130002, 'Knoblauch', '2 Zehen');

-- Schoko-Brownies
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a683-5d3e-11ec-bf63-0242ac130002, 'Butter', '200g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a683-5d3e-11ec-bf63-0242ac130002, 'Schokolade', '200g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a683-5d3e-11ec-bf63-0242ac130002, 'Zucker', '250g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a683-5d3e-11ec-bf63-0242ac130002, 'Eier', '4 Stück');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a683-5d3e-11ec-bf63-0242ac130002, 'Mehl', '150g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a683-5d3e-11ec-bf63-0242ac130002, 'Kakao', '50g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a683-5d3e-11ec-bf63-0242ac130002, 'Backpulver', '1 TL');

-- Tomatensuppe
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a684-5d3e-11ec-bf63-0242ac130002, 'Tomaten', '800g');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a684-5d3e-11ec-bf63-0242ac130002, 'Zwiebeln', '1 Stück');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a684-5d3e-11ec-bf63-0242ac130002, 'Knoblauch', '2 Zehen');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a684-5d3e-11ec-bf63-0242ac130002, 'Gemüsebrühe', '500ml');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a684-5d3e-11ec-bf63-0242ac130002, 'Salz', 'nach Geschmack');
INSERT INTO zutaten (zutat_id, rezept_id, name, menge) VALUES (uuid(), e1d6a684-5d3e-11ec-bf63-0242ac130002, 'Pfeffer', 'nach Geschmack');
