#!/bin/bash

# Backup script
echo "Starting backup..."
nodetool snapshot rezept_sammlung -t backup_snapshot
echo "Backup completed."

# Restore script
echo "Starting restore..."
# Clear current data
rm -rf /var/lib/cassandra/data/rezept_sammlung
# Move snapshot files back to data directory
cp -r /var/lib/cassandra/data/rezept_sammlung/snapshots/backup_snapshot/* /var/lib/cassandra/data/rezept_sammlung
echo "Restore completed."
