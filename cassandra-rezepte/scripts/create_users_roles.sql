-- Use the keyspace
USE rezept_sammlung;

-- Create roles only if they do not exist
CREATE ROLE IF NOT EXISTS rezept_admin WITH PASSWORD = 'admin123' AND SUPERUSER = true AND LOGIN = true;
CREATE ROLE IF NOT EXISTS rezept_user WITH PASSWORD = 'user123' AND LOGIN = true;

-- Grant permissions only if not already granted
GRANT ALL PERMISSIONS ON KEYSPACE rezept_sammlung TO rezept_admin;
GRANT SELECT ON rezept_sammlung.rezepte TO rezept_user;
GRANT SELECT ON rezept_sammlung.zutaten TO rezept_user;
